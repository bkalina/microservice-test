package pl.javamylove.microservice.product;

import javax.inject.Named;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

/**
 * @author bartosz.kalina@pentegy.com
 */
@Configuration
public class ApplicationConfig {

  @Named
  static class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
      this.packages("pl.javamylove.microservice.product.rest");
    }
  }
}
