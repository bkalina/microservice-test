package pl.javamylove.microservice.product.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import pl.javamylove.microservice.model.product.Product;

/**
 * @author bartosz.kalina@pentegy.com
 */
@Path("")
public class ProductRest {

  private static List<Product> products = new ArrayList<>();

  static {
    products.add(new Product() {{
      setId(200);
      setSku("LOL");
      setDescription("Trololo");
    }});
  }

  @GET
  @Path("/products")
  @Produces(MediaType.APPLICATION_JSON)
  public static List<Product> getProducts() {
    return products;
  }

  @GET
  @Path("/product")
  @Produces(MediaType.APPLICATION_JSON)
  public Product getProduct(@QueryParam("id") long id) {
    return products.stream()
        .filter(p -> p.getId() == id)
        .findAny()
        .orElse(null);
  }
}
