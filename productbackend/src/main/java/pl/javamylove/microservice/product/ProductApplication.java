package pl.javamylove.microservice.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * @author bartosz.kalina@pentegy.com
 */
@SpringBootApplication
public class ProductApplication extends SpringBootServletInitializer {

  public static void main(String[] args) {
    SpringApplication.run(ProductApplication.class, args);
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(ProductApplication.class);
  }
}
