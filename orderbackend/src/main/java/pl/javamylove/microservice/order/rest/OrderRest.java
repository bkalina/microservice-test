package pl.javamylove.microservice.order.rest;

import java.util.Date;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.web.client.RestTemplate;

import pl.javamylove.microservice.model.customer.Customer;
import pl.javamylove.microservice.model.order.Order;
import pl.javamylove.microservice.model.product.Product;

/**
 * @author bartosz.kalina@pentegy.com
 */
@Path("")
public class OrderRest {

  private long id = 1;

  @Inject
  private RestTemplate restTemplate;

  @GET
  @Path("/order")
  @Produces(MediaType.APPLICATION_JSON)
  public Order submitOrder(@QueryParam("customerId") long customerId,
      @QueryParam("productId") long productId,
      @QueryParam("amount") long amount) {
    Order order = new Order();
    Customer customer = restTemplate.getForObject(
        "http://192.168.0.20:8080/customer/customer?id={id}", Customer.class,
        customerId);
    Product product = restTemplate.getForObject(
        "http://192.168.0.30:8080/product/product?id={id}", Product.class,
        productId);
    order.setCustomer(customer);
    order.setProduct(product);
    order.setId(id);
    order.setAmount(amount);
    order.setDateOrder(new Date());
    id++;
    return order;
  }
}
