package pl.javamylove.microservice.customer;

import javax.inject.Named;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author bartosz.kalina@pentegy.com
 */
@Configuration
public class ApplicationConfig {

  @Named
  static class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
      this.packages("pl.javamylove.microservice.customer.rest");
    }
  }
}
