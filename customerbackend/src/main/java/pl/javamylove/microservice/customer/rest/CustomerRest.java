package pl.javamylove.microservice.customer.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import pl.javamylove.microservice.model.customer.Customer;

/**
 * @author bartosz.kalina@pentegy.com
 */
@Path("")
public class CustomerRest {

  private static List<Customer> customers = new ArrayList<Customer>();

  static {
    customers.add(new Customer() {{
      setId(100);
      setName("edek");
      setEmail("edek@wp.pl");
    }});
  }

  @GET
  @Path("/customers")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Customer> getCustomers() {
    return customers;
  }

  @GET
  @Path("/customer")
  @Produces(MediaType.APPLICATION_JSON)
  public Customer getCustomer(@QueryParam("id") long id) {
    return customers.stream()
        .filter(c -> c.getId() == id)
        .findAny()
        .orElse(null);
  }
}
